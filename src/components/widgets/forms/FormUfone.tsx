import type { FC } from 'react';
import {
  Autocomplete,
  Box,
  Button,
  CardActions,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  Switch,
  TextField,
  // Typography,
  Checkbox,
  Radio, RadioGroup, FormControlLabel, FormControl,
  //  FormLabel,
  CircularProgress
} from '@material-ui/core';

import AdapterDateFns from '@material-ui/lab/AdapterDateFns';
import LocalizationProvider from '@material-ui/lab/LocalizationProvider';
import DateTimePicker from '@material-ui/lab/DateTimePicker';
// import MobileDateTimePicker from '@material-ui/lab/MobileDateTimePicker';
// import DesktopDateTimePicker from '@material-ui/lab/DesktopDateTimePicker';


const countries = [
  { text: 'Jersey', value: 'JE' },
  { text: 'Jordan', value: 'JO' },
  { text: 'Kazakhstan', value: 'KZ' },
  { text: 'Kenya', value: 'KE' },
  { text: 'Kiribati', value: 'KI' },
  { text: 'Korea, Democratic People\'S Republic of', value: 'KP' },
  { text: 'Korea, Republic of', value: 'KR' },
  { text: 'Kuwait', value: 'KW' },
  { text: 'Kyrgyzstan', value: 'KG' },
  { text: 'Lao People\'S Democratic Republic', value: 'LA' }
];

const FormUfone: FC = () => (
  <Box
    sx={{
      backgroundColor: 'background.paper',
      minHeight: '100%',
      p: 3
    }}
  >
    <form onSubmit={(event) => event.preventDefault()}>
      <CardHeader title="Title" />
      <CardContent>
        <Grid
          container
          spacing={4}
        >

          <Grid
            item
            md={4}
            xs={6}
          >
            <Autocomplete
              getOptionLabel={(option): string => option.text}
              options={countries}
              renderInput={(params): JSX.Element => (
                <TextField
                  fullWidth
                  label="Auto Complete"
                  name="country"
                  variant="outlined"
                  {...params}
                />
              )}
            />
          </Grid>
          <Grid
            item
            md={4}
            xs={6}
          >
            <TextField
              fullWidth
              label="Input field"
              name="name"
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={4}
            xs={6}
          >
            <TextField
              fullWidth
              label="Input field disabled"
              name="name"
              variant="outlined"
              disabled
            />
          </Grid>
          <Grid
            item
            md={4}
            xs={6}
          >
            <TextField
              fullWidth
              label="Multiline"
              name="name"
              variant="outlined"
              multiline
              rows={4}

            />
          </Grid>
          <Grid
            item
            md={4}
            xs={6}
          >
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DateTimePicker
                renderInput={(props) => <TextField {...props} />}
                label="DateTimePicker"
                value={new Date()}
                onChange={(newValue) => {
                  console.log(newValue);
                }}
              />
            </LocalizationProvider>
          </Grid>
          <Grid
            item
            md={4}
            xs={6}
          >
            <TextField
              fullWidth
              helperText="We will use this email to contact you"
              label="Email Address"
              name="email"
              required
              type="email"
              variant="outlined"
            />
          </Grid>
          <Grid
            item
            md={4}
            xs={6}
          >
            <Button
              color="primary"
              type="submit"
              variant="contained"
            >
              Button
            </Button>
          </Grid>
          <Grid
            item
            md={4}
            xs={6}
          >
            <Checkbox defaultChecked />

          </Grid>
          <Grid
            item
            md={4}
            xs={6}
          >
            <FormControl component="fieldset">
              <RadioGroup row aria-label="gender" name="row-radio-buttons-group">
                <FormControlLabel value="female" control={<Radio />} label="Female" />
                <FormControlLabel value="male" control={<Radio />} label="Male" />
                <FormControlLabel value="other" control={<Radio />} label="Other" />
              </RadioGroup>
            </FormControl>
          </Grid>
          <Grid
            item
            md={4}
            xs={6}
          >
            <Switch
              color="primary"
              edge="start"
              name="isPublic"
            />
          </Grid>
          <Grid
            item
            md={4}
            xs={6}
          >
            <CircularProgress />
          </Grid>
        </Grid>
      </CardContent>
      <Divider />
      <CardActions
        sx={{
          justifyContent: 'flex-end',
          p: 2
        }}
      >
        <Button
          color="primary"
          type="submit"
          variant="contained"
        >
          Save Settings
        </Button>
      </CardActions>
    </form>
  </Box>
);

export default FormUfone;
