import { useEffect } from 'react';
import type { FC } from 'react';
import { Helmet } from 'react-helmet-async';
import { Box, Container } from '@material-ui/core';
import FormUfone from '../../components/widgets/forms/FormUfone';
import TableUfone from '../../components/widgets/tables/TableUfone';
import UfoneList from '../../components/widgets/grid-lists/UfoneList';
import UfoneTabs from '../../components/widgets/tabs/UfoneTabs';
import WidgetPreviewer from '../../components/WidgetPreviewer';
import gtm from '../../lib/gtm';

const UfoneFormControls: FC = () => {
  useEffect(() => {
    gtm.push({ event: 'page_view' });
  }, []);

  return (
    <>
      <Helmet>
        <title>Browse: Forms | Material Kit Pro</title>
      </Helmet>
      <Box
        sx={{
          backgroundColor: 'background.default',
          minHeight: '100%',
          py: 8
        }}
      >
        <Container maxWidth="lg">
          <Box>
            <WidgetPreviewer
              element={<TableUfone />}
              name="Grid"
            />
          </Box>
          <Box>
            <WidgetPreviewer
              element={<UfoneList />}
              name="Transfer List"
            />
          </Box>
          <Box>
            <WidgetPreviewer
              element={<UfoneTabs />}
              name="Tabs"
            />
          </Box>
          <Box>
            <WidgetPreviewer
              element={<FormUfone />}
              name="Form Controls"
            />
          </Box>
        </Container>
      </Box>
    </>
  );
};

export default UfoneFormControls;
